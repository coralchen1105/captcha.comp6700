package captcha.comp6700.svgfontreader;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

import javafx.scene.image.Image;


public class SvgFontReader extends DefaultHandler {

	
    private Map<Character, String> glyphs;

    private static ArrayList<Character> charSet;
      
    public static ArrayList<Character> getCharSet() {
        return charSet;
    }

    // initialize when class load
    static {
        charSet = new ArrayList<Character>();
        for (char c = '0'; c <= '9'; c++) {
            charSet.add(c);
        }
        for (int i = (int) 'a'; i <= (int) 'z'; i++) {
            charSet.add((char) i);
        }
    }

    public Map<Character, String> getGlyphs() {
        return glyphs;
    }

 
    // loop to search character
    public String get(char c) throws CharNotRepresented {
        // use Map (glyphs object data structure)'s get() function. Not recursion
        // return the co-responding key value 
        String str = this.getGlyphs().get(c);
        if (str != null)
            return str;
        else
            throw new CharNotRepresented(c);
    }
    
    public SvgFontReader() throws SAXException, IOException {
    	 super();
  
        ArrayList<String> allfiles = getAllFiles();
        String fn = randomSelectFile(allfiles);
        System.out.println(fn);
        
        glyphs = new HashMap<Character, String>();
        XMLReader xr = XMLReaderFactory.createXMLReader();
        /* keep the next two statements to avoid dtd validation:
         * it'll allow to run off-line, and 'll be lot quicker */
        xr.setFeature(
                "http://apache.org/xml/features/nonvalidating/load-external-dtd",
                false);
        xr.setFeature("http://xml.org/sax/features/validation", false);
        
        xr.setContentHandler(this);
        xr.setErrorHandler(this);
        xr.setDTDHandler(this);
        
        // read file content and parse to xr objects
        
        // TODO
        // get absolute path
        String path = "svgfonts/" + fn;
        
        FileReader fr = new FileReader(path);
        xr.parse(new InputSource(fr));
        
    }

    
    public ArrayList<String> getAllFiles(){
        File file = new File("svgfonts.properties");
        
        ArrayList<String> fileName = new ArrayList<String>();
        
        try {
            Scanner scanner = new Scanner(file);
 
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                String[] eachLine = line.split("=");
                fileName.add(eachLine[1]);
            
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        
        return fileName;
        
    }
    
    public String randomSelectFile(ArrayList<String> files){
        Random randomGenerator = new Random();
        int index = randomGenerator.nextInt(files.size());
        return files.get(index);
        
    }
        
    @Override 
    // override defaultHandler interface and call back to 
    // ContentHandler registered event: xr
    public void startDocument() {
        //System.out.println("Start parsing document:");
        System.out.print("start document");
    }

 
    @Override
    public void endDocument() {
        System.out.println(" end document");
    }
    
    @Override  
    public void startElement(String uri, String name,
                             String qName, Attributes atts) {
        // System.out.println("The tag name is " + name);
        // value for the attribute "unicode" (which is glyph's unicode symbol)
        
        String unicodeValue = atts.getValue("unicode");
        // value for the attribute "d" (which is glyph's svg-path)
        String pathValue = atts.getValue("d");
        char c;

        if (name.equals("glyph") && unicodeValue != null) {
            c = unicodeValue.charAt(0);

            if (charSet.contains(c) && pathValue != null) {
                glyphs.put(c, pathValue);
            }
        }
    }

}
