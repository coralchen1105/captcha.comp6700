package captcha.comp6700.captchafx;

import static java.lang.Math.max;
import static java.lang.Math.min;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javafx.animation.Interpolator;
import javafx.animation.ParallelTransition;
import javafx.animation.PathTransition;
import javafx.animation.RotateTransition;
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.SceneBuilder;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.*;
import javafx.scene.image.WritableImage;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.SVGPath;
import javafx.scene.text.Font;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Scale;
import javafx.scene.transform.Translate;
import javafx.stage.Stage;
import javafx.util.Duration;

import javax.imageio.ImageIO;

import org.xml.sax.SAXException;

import captcha.comp6700.svgfontreader.CharNotRepresented;
import captcha.comp6700.svgfontreader.SvgFontReader;
import captcha.comp6700.captchariser.*;

public class CaptchaFX extends Application { 
    private Group root;
    
    private Translate translate;
    private SvgFontReader model;
    private Rendered render;
    private static ArrayList<Character> selectedChar; 
    private static ArrayList<String> selectedSVG;
    
    private SVGPath currentCharPath;
    private Group letterEnclosure;

    
    public static void main(String[] args)
            throws IOException, SAXException {
        Application.launch(args);
        
    }

    @Override
    public void start(final Stage primaryStage) {
        primaryStage.setTitle("SVG Captcha");
        try {
            model = new SvgFontReader();
            render = new Rendered(SvgFontReader.getCharSet());    
            selectedChar = render.getSelectedGlyphs();
           
        } catch (SAXException e) {
            System.err.print("cannot parse the file %s%n");
            System.exit(1);
        } catch (IOException e) {
//            System.err.printf("cannot open the file %s%n", fontFileName);
            System.exit(2);
        }

        /* the main window */
        root = new Group();

        final Scene scene = new Scene(root, 500, 250, Color.WHITE);

        AnchorPane anchorpane = new AnchorPane();
        anchorpane.setPrefSize(400, 250);
        
        Label label = new Label("SVG Captcha");
        label.setFont(new Font("Arial", 15));
        Transformer.setPosition(label, 200.0, 10.0);
        
        Button newCap = new Button("new captcha");
        Button solve = new Button("solve");
        Button quit = new Button("quit");
        
        Transformer.setPosition(newCap, 119.0, 49.0);
        Transformer.setPosition(solve, 249.0, 49.0);
        Transformer.setPosition(quit, 367.0, 49.0);
        
        HBox btGroup = new HBox(30);
        btGroup.getChildren().addAll(newCap, solve, quit);
        btGroup.setLayoutX(119.0);
        btGroup.setLayoutY(49.0);
        Transformer.setPosition(btGroup, 119.0, 49.0);
        
        final TextField textField = new TextField ();
        Transformer.setPosition(textField, 120.0, 200.0);
        
        final Pane canvas = new Pane();
        canvas.setStyle("-fx-background-color: grey;");
        canvas.setPrefSize(270,100);
        Transformer.setPosition(canvas, 119.0, 89.0);
        
        selectedSVG = getSelectedSVG(selectedChar);
        
        Transformer.displaySVG(selectedSVG,canvas);
     // button event handler
        newCap.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
            	try {
					model = new SvgFontReader();
				} catch (SAXException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            	render = new Rendered(SvgFontReader.getCharSet());    
                selectedChar = render.getSelectedGlyphs();
                selectedSVG = getSelectedSVG(selectedChar);
//                StringBuilder st = new StringBuilder();
//                for(Character c: selectedChar){
//                	st.append(c);
//                }
//                System.out.println(st);
                canvas.getChildren().clear();
                Transformer.displaySVG(selectedSVG,canvas);
            }
        });
        
        solve.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
            	String text = textField.getText();
            	
            	StringBuilder st = new StringBuilder();
            	for(Character c: selectedChar){
            		st.append(c);
            	}
            	
            	String charSet = st.toString();
            	
            	if(text.equals(charSet)){
            		System.out.println("Match");
            	}else{
            		System.out.println("Not Match");
            	}
              
            }
        });
        
        quit.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
            primaryStage.close();
            }
        });
        
        
        AnchorPane.setTopAnchor(label, 10.0);
        anchorpane.getChildren().addAll(label,btGroup,textField,canvas);
        root.getChildren().add(anchorpane);
        primaryStage.setScene(scene);
        primaryStage.show();
      
    }
    
    // get selectedSVG from model class
    public ArrayList<String> getSelectedSVG(ArrayList<Character> selectedChar){
    	ArrayList<String> selectedSVG = new ArrayList<String>();
    	
    	for(Character c: selectedChar){
      	   String charCode = model.get(c); 
      	   selectedSVG.add(charCode);
         }
    	
    	return selectedSVG;
    }
}
