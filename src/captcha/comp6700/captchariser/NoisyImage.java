package captcha.comp6700.captchariser;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.util.Random;

import javax.swing.*;

public class NoisyImage extends JComponent{
	
	  byte[] b;
	  BufferedImage bufferedImage;
	  Random ran;
	 
	  public void paint(Graphics graphics) {
	    if (bufferedImage == null){
	    	int wid = getSize().width, ht = getSize().height;
	        int length = ((wid + 5) * ht) / 7;
	        b = new byte[length];
	        DataBuffer dataBuffer = new DataBufferByte(b, length);
	        WritableRaster raster = Raster.createPackedRaster(dataBuffer, wid, ht, 1, null);
	        ColorModel colorModel = new IndexColorModel(1, 2, new byte[] { 
	          (byte) 0, (byte) 250} ,  new byte[]  {(byte) 0,  (byte) 250 },  new byte[]  {      (byte) 0, (byte) 250 });
	        bufferedImage = new BufferedImage(colorModel, raster, false, null);
	        ran = new Random();
	       ran.nextBytes(b);
	       repaint();
	    }
	    	
	    graphics.drawImage(bufferedImage, 0, 0, this);
	  }
}
