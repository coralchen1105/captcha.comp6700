package captcha.comp6700.captchariser;

import java.util.ArrayList;

import javafx.geometry.Bounds;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.effect.*;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.shape.FillRule;
import javafx.scene.shape.SVGPath;
import javafx.scene.shape.Shape;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Scale;



public class Transformer {
    
	static final int scaleFactor = 40;
    static final int kerning = 3;
    
	// TODO implement this class and use it to distort rendered glyphs
	public static void setPosition(Node n, Double x, Double y){
    	n.setLayoutX(x);
    	n.setLayoutY(y);
    }
		
	public static Group changeFontSize(String charCode){
    	
    	Bounds bb;
    	
        SVGPath currentCharPath = new SVGPath();
        currentCharPath.setContent(charCode);
        
        Group letterEnclosure = new Group(currentCharPath);
       
        bb = currentCharPath.getLayoutBounds();
        double xSize = bb.getWidth();
        double ySize = bb.getHeight();
       
        double size = (xSize + ySize)/2;
//        currentCharPath.setFillRule(FillRule.EVEN_ODD);
//        
//        currentCharPath.setFill(Color.DARKRED);
        //currentCharPath.setStyle("-fx-fill: rgba(0,255,0,0.3);");
        currentCharPath.setStrokeWidth(0);
       
        Scale scale = new Scale(scaleFactor / size,scaleFactor / size);
        
        currentCharPath.getTransforms().add(scale);  
        
        setSVGEffect(currentCharPath);
        
        return letterEnclosure;
    }
	
	public static void setSVGEffect(Shape shape){
		
        
		MotionBlur motionBlur = new MotionBlur();
        motionBlur.setRadius(600);
        motionBlur.setAngle(-5.0);
        
        shape.setEffect(motionBlur);
		
        Image img = new Image("noisyimage.png");
        shape.setFill(new ImagePattern(img,0,0,10,10,true));
        
        int width =(int) shape.getLayoutBounds().getWidth();
        
        int height = (int) shape.getLayoutBounds().getHeight();
        
        double angelR = Math.random()*Math.PI;
        
        FloatMap floatMap = new FloatMap();
        floatMap.setWidth(width);
        floatMap.setHeight(height);

        for (int i = 0; i < width; i++) {
        	double a1 = Math.PI * i/150 + angelR;
            double v = (Math.sin(a1) )/ 20.0;
           
            for (int j = 0; j < height; j++) {
                floatMap.setSamples(i, j, 0.0f, (float) v);
            }
        }

        DisplacementMap displacementMap = new DisplacementMap();
        displacementMap.setMapData(floatMap);
        
        shape.setEffect(displacementMap);
	}
	
	 public static void rotation(Group g){
	    g.setRotationAxis(Rotate.X_AXIS);
	    g.setRotate(180);
	 }
	    
	 public static void displaySVG(ArrayList<String> selectedSVG, Pane n){
	    	
	    Double num = 5.0;
	        
	    for(String s: selectedSVG){     	
	        	
	    	Group eachFont = Transformer.changeFontSize(s);
	    	
	        rotation(eachFont);
	        Transformer.setPosition(eachFont, num, 20.0);
	        num+= 40.0;
	        
	        
	        n.getChildren().add(eachFont);
	        
	        
	    }
	        
	  }
	    
}
