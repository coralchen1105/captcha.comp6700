package captcha.comp6700.captchariser;

import java.util.*;


/**
 * Created with IntelliJ IDEA.
 * User: abx
 * Date: 25/04/2014
 * Time: 6:38 PM
 * Created for Assignment Two, COMP6700.2014, ANU, RSCS
 * @version 1.0
 * @author abx
 * @author (your name and id)
 * @see captchafx.CaptchaFX
 */

public class Rendered {

    // TODO implement this class if you want to render glyphs
    // TODO separately from the main class captchafx.CaptchaFX
	
	private ArrayList<Character> selectedChar;
	
	public ArrayList<Character> getSelectedGlyphs() {
		return selectedChar;
	}


	public Rendered(ArrayList<Character> charSet){
		
		this.selectedChar = selectCharacter(charSet);
	}
	
//	public Map<Character, String> randomSelectGlyphs(Map<Character, String> glyphs, ArrayList<Character> selectedSet){
//		Map<Character, String> selectedGlyphs = new HashMap<Character, String>();
//		
//		for(Character c: selectedSet){
//			if(glyphs.containsKey(c)){
//				selectedGlyphs.put(c, glyphs.get(c));
//			}
//		}
//		
//		return selectedGlyphs;
//	}
	
	public ArrayList<Character> selectCharacter(ArrayList<Character> charSet){
		
		ArrayList<Character> selectedChar = new ArrayList<Character>();
		Collections.shuffle(charSet);
		for(int i=0; i<6; i++){
			selectedChar.add(charSet.get(i));
		}
		
		return selectedChar;
	}

}
